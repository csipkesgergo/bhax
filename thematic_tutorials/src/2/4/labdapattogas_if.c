#include <stdio.h>
#include <curses.h>
#include <unistd.h>

int main ( void ) {

    // Mutató az aktuális kurzorablakra
    WINDOW *ablak;
    ablak = initscr ();

    // Labda kezdő pozíció
    int x = 0;
    int y = 0;

    // Lépés mértéke
    int xnov = 1;
    int ynov = 1;

    // Ablak mérete
    int mx;
    int my;

    for ( ;; ) {

        // Beállítjuk az ablak aktuális méretét
        getmaxyx ( ablak, my , mx );

        // Megjelenítjük a labdát
        mvprintw ( y, x, "O" );

        // Ablak frissítése
        refresh ();

        // msec-ben megadjuk, hogy meddig aludjon a program (50000 < x < 150000)
        usleep ( 10000 );
        
        // Töröljük az eddig kirajzoltakat
        clear();

        // Labda mozgatása
        x = x + xnov;
        y = y + ynov;

        // Ha elértük a felület jobb oldalát, visszafordítjuk
        if ( x>=mx-1 ) {
            xnov = xnov * -1;
        }
        // Ha elértük a felület bal oldalát, visszafordítjuk
        if ( x<=0 ) {
            xnov = xnov * -1;
        }
        // Ha elértük a felület tetejét, visszafordítjuk
        if ( y<=0 ) {
            ynov = ynov * -1;
        }
        // Ha elértük a felület alját, visszafordítjuk
        if ( y>=my-1 ) {
            ynov = ynov * -1;
        }
    }
    return 0;
}