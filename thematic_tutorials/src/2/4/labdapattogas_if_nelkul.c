#include<stdlib.h>
#include<time.h>

// Kurzor pozícionálás
static void gotoxy(int x, int y) {
    int i;

    // Lefelé irányítás
    for(i=0; i<y; i++) printf("\n");
    // Jobbra irányítjuk
    for(i=0; i<x; i++) printf(" ");

    // Labda kirajzolása
    printf("o\n");

}

void usleep(int);

int main(void) {
  
    int egyx=1;
    int egyy=-1;
    int i;

    // Beállítjuk alabda kezdő pozícióját
    int x=10;
    int y=20;

    // Beállítjuk a megjelenítő felület méretét
    int ty[40];
    int tx[60];

    // Pálya széleinek meghatározása
    for(i=0; i<40; i++)
        ty[i]=1;

    ty[1]=-1;
    ty[49]=-1;

    for(i=0; i<59; i++)
        tx[i]=1;

    tx[1]=-1;
    tx[59]=-1;


    for( ; ; ) {
        (void)gotoxy(x,y);

        x+=egyx;
        y+=egyy;

        egyx*=tx[x];
        egyy*=ty[y];

        usleep (100000);
        (void)system("clear");
    }
}