#include <stdio.h>

// Két változó értékének cseréje segdváltozó használatával
void swapWidthTmp(int a, int b) {
	int tmp;

	tmp = a;
	a = b;
	b = tmp;

	printf("Csere segedvaltozo hasznalataval:\n");
	printf("a=%d\nb=%d", a, b);
}

// Két változó cseréje aritmetikai műveletekkel
void swapWidthArithmetic(int a, int b) {
	b = b - a;
    a = a + b;
    b = a - b;

    printf("Csere aritmetikai muveletekkel:\n");
    printf("a=%d\nb=%d", a, b);

}

int main() {

	int a = 10;
	int b = 20;

	printf("Kezdeti valtozok:\na=%d\nb=%d\n\n", a, b);

	swapWidthTmp(a, b);
	printf("\n\n");

	swapWidthArithmetic(a, b);

}

