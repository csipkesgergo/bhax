#include <stdio.h>

int main() {
    int szo = 1;
    int szohossz = 0;
    
    do {
        szohossz++;
    } while(szo <<= 1);
    
    printf("A szo ezen a gepen %d bites.\n", szohossz);
    
    return 0;
}
