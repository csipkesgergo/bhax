#include <iostream>
using namespace std;

int main() {
    int a;
    int cnt = 0;
    cout << "Adj meg egy decimalis szamot!: ";
    cin >> a;
    cout << "A szam unaris formaban:\n";
    
    for (int i = 0; i < a; ++i) {
        cout << "|";
        cnt++;
        if (cnt % 5 == 0) {
            cout << " ";
        }
    }

    cout<<'\n';
    
    return 0;
}